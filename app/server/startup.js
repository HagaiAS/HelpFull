Meteor.startup(function() {

    Accounts.onCreateUser(function (options, user) {

        if (options.profile) {
            // include the user profile
            user.profile = options.profile;
        }

        var gravatarUrl = 'http://res.cloudinary.com/dse3cdgvi/image/upload/v1464624928/Default-avatar_zs7idf.jpg';

        // other user object changes...
        user.profile.picture = gravatarUrl;

        return user;
    });

});