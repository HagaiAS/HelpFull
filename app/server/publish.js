Meteor.publish('Contacts', function() {
    return Contacts.find();
});

Meteor.publish('Teams', function (){
    return Teams.find({});
});

Meteor.publish('Users', function (){
    return Users.find({});
});

Meteor.publish('Tasks', function (){
    return Tasks.find({});
});

Meteor.publish('Lists', function (){
    return Lists.find({});
});

Meteor.publish('Comments', function (){
    return Comments.find({});
});

Meteor.publish('singleUser', function(userId) {
    check(userId, String);
    return Users.find({_id: userId});
});