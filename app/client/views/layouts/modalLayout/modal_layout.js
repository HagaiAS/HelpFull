Template.modalLayout.rendered = function(){

// Show the example modal directly on startup...

    $('body').addClass('md-skin');
    
    // adding the rtl
    $('body').addClass('rtls');

    // Move modal to body
    // Fix Bootstrap backdrop issu with animation.css
    $('.modal').appendTo("body");
    $('#myModalLayout').modal('show');
};