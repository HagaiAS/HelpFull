Template.pageHeading.helpers({

    // Route for Home link in breadcrumbs
    home: 'home',
    homeTitle: function () {
        return TAPi18n.__('home_page.title');
    },
});