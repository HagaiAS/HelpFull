Template.nav.rendered = function () {

    // Initialize metisMenu
    $('#side-menu').metisMenu();
};

Template.nav.helpers({
    userEmail: function () {
        return this.emails[0].address;
    }
});

