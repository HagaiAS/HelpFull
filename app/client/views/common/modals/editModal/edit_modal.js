Template.editModal.helpers({
    editTitle: function () {
        return TAPi18n.__('common.edit_title');

    },
    editBtn: function () {
        return TAPi18n.__('common.edit_save_btn');
    }
});