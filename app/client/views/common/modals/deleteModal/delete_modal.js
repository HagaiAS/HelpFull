Template.deleteModal.helpers({
    onError: function () {
        return function (error) {
            alert("BOO!");
            console.log(error);
        };
    },
    onSuccess: function () {
        return function (result) {
            alert("YAY!");
            console.log(result);
        };
    },
    beforeRemove: function () {
        return function (collection, id) {
            var doc = collection.findOne(id);
            var deleteContact = TAPi18n.__('common.delete_message');
            if (confirm(deleteContact + doc.name + '?')) {
                this.remove();
            }
        };
    }
});
