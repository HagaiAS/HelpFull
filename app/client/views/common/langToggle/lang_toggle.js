var setActive = function(lang) {
    lang = lang ? lang : TAPi18n.getLanguage();
    $('.lang').removeClass('active');
    $('.js-lang-' + lang).addClass('active');
};

var setLang = function(lang) {
    TAPi18n.setLanguage(lang);
    mo.setLocale(lang);
    T9n.setLanguage(lang);
    setActive(lang);
};

Template.langToggle.rendered = function(){

    $('.js-lang-en a').on('click', function (){
        setLang('en');
    });

    $('.js-lang-he a').on('click', function (){
        setLang('he');
    });
};