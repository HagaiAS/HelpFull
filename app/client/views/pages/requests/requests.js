Template.requests.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Teams');
        self.subscribe('Users');
        self.subscribe('Lists');
        self.subscribe('Contacts');
        self.subscribe('Tasks');
    });
});

Template.requests.helpers({

    requestsTitle: function () {
        return TAPi18n.__('requests_page.title');
    },
    createRequest: function() {
        return {
            buttonId: 'addRequest',
            buttonText: TAPi18n.__('requests_page.add_request'),
            collection: 'Tasks',
            saveBtn: TAPi18n.__('common.add_save_btn'),
            title: TAPi18n.__('requests_page.add_request')
        };
    }

});