Template.profile.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var userId = FlowRouter.getParam('userId');
        self.subscribe('Users');
    });
});

Template.profile.helpers({
    categoryTitle: function () {
        return {
            title: TAPi18n.__('profile_page.title'),
            path: "/profile"
        }
    },
    userTitle: function () {
        return TAPi18n.__('profile_page.user_title');
    },
    user: function() {
        var userId = FlowRouter.getParam('userId');
        if (!userId)
        {
            userId = Meteor.user()._id;
        }
        return Users.findOne(userId);
    }
});