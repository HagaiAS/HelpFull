Template.profileDetails.helpers({
    userEmail: function (user) {
        return user.emails[0].address;
    }
});