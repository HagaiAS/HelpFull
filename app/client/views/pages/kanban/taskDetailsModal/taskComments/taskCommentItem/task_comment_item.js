Template.taskCommentItem.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Users');
    });
});

Template.taskCommentItem.helpers({
    user: function (userId) {
        var user = Users.findOne(userId);
        return user;
    }    
});
