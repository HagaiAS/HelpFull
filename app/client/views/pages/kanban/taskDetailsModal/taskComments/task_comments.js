Template.taskComments.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Comments');
    });
});

Template.taskComments.helpers({
    currentComments: function (taskId) {
        var result =  Comments.find({task_id: taskId}, {sort: {created_at: -1}});
        Session.set('count_comments#'+taskId, result.count());
        return result;
    },
    countComments: function (taskId) {
        var count = Session.get('count_comments#'+taskId);
        return count;
    }
});