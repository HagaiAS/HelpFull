Template.addTaskComments.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Users');
        self.subscribe('Comments');
    });
});

Template.addTaskComments.helpers({
    textPlaceholder: function () {
        return TAPi18n.__('task_comments_modal.textPlaceholder');        
    }
});
