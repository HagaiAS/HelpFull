Template.taskModalFooter.rendered = function(){
    $('body').addClass('md-skin');

    // adding the rtl
    $('body').addClass('rtls');
};

Template.taskModalFooter.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var taskId = FlowRouter.getParam('taskId');
    });
});

Template.taskModalFooter.helpers({
    currentTaskId: function () {
        var task_id = FlowRouter.getParam('taskId');
        return task_id;
    }
});