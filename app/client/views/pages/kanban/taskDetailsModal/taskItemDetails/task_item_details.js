Template.taskItemDetails.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Users');
        self.subscribe('Tasks');
        self.subscribe('Teams');
        self.subscribe('Contacts');
    });
});

Template.taskItemDetails.helpers({
        contactName: function() {
        return Contacts.findOne(this.need_assistance_id).name;
    },
    userName: function(user_id) {
        return Users.findOne(user_id).profile.name;
    },
    teamName: function(team_id) {
        return Teams.findOne(team_id).name;
    },
    taskDescription: function() {
        return {
            collection: "tasks",
            field: "description",
            textarea: true,
            acceptEmpty: true,
            placeholder: "הוסף תיאור ..",
            substitute: 'הוסף תיאור',
            title: "תיאור"
        }
    }
});

// Template.taskItemDetails.events({
//     'click .js-back': function () {
//         $('#afUpdateTaskDetails').submit()
//     }
// });