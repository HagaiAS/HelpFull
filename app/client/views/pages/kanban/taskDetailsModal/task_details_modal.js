Template.taskDetailsModal.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var taskId = FlowRouter.getParam('taskId');
        self.subscribe('Tasks');
    });
});

Template.taskDetailsModal.helpers({
    currentTask: function () {
        var task_id = FlowRouter.getParam('taskId');
        var currentTask = Tasks.findOne(task_id);
        return currentTask;
    }
});