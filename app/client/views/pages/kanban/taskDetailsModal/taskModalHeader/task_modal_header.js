// taskDetailsModal
Template.taskModalHeader.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var taskId = FlowRouter.getParam('taskId');
        self.subscribe('Tasks');
    });
});

Template.taskModalHeader.helpers({
    currentTask: function () {
        var task_id = FlowRouter.getParam('taskId');
        var currentTask = Tasks.findOne(task_id);
        return currentTask;
    },
    backKanbanTeam: function () {
        return '/kanban/' + this.team_id;
    },
    headerTitle: function () {
        var result = this.title;
        return result;
    },
    headerLabel: function () {
        var result;
        var labelText = TAPi18n.__("team_kanban_page.is_urgent_tag");
        if(this.is_urgent)
        {
            result = '<span class="pull-right flip label label-danger">' + labelText + '</span>';
        }
        return result
    }
});