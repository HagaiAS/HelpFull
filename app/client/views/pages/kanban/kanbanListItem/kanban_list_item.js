Template.kanbanListItem.onRendered(function() {
    // Initialize sortable
    var currentList = Template.currentData()._id;
    var container = document.getElementById(currentList);
    var sort = Sortable.create(container, {
        animation: 200, // ms, animation speed moving items when sorting, `0` — without animation
        // handle: ".drag-task", // Restricts sort start click/touch to the specified element
        group: "sorting",
        sort: true,
        // Changed sorting within list
        onUpdate: function (event){
            var taskId = event.item.id;
            var newSort = event.newIndex;
            Tasks.update(taskId, {$set:{"sort": newSort}});
        },
        // Element is dropped into the list from another list
        onAdd: function (event){
            const taskId = event.item.id;
            const listId = event.to.id;
            const newSort = event.newIndex;
            Tasks.update(taskId, {$set:{"list_id": listId, "sort": newSort}});
        }
    });
});

Template.kanbanListItem.helpers({
    tasks: function (listId) {
        var result = Tasks.find({ list_id: listId }, { sort: { sort: 1 }});
        Session.set('count_task#'+listId, result.count());
        return result;
    },
    countTasks: function (listId) {
        var count = Session.get('count_task#'+listId);
        return count;
    }
});

Template.kanbanListItem.events({
    'submit .new-task'(event) {
        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.target;
        const listId = target.id;
        const text = target.text.value;
        const teamId = Template.currentData().team_id;
        const taskBordersColor = "info-element";
        // Insert a task into the collection
        Tasks.insert({ title: text, team_id: teamId, list_id: listId , task_borders_color: taskBordersColor});

        // Clear form
        target.text.value = '';
    },
    'click .add-task-icon'(event) {

        // Prevent default browser form submit
        event.preventDefault();

        // Get value from form element
        const target = event.currentTarget;
        const listId = target.parentNode.id;
        const text = target.parentNode[0].value;
        const teamId = Template.currentData().team_id;
        const taskBordersColor = "info-element";

        // Insert a task into the collection
        Tasks.insert({ title: text, team_id: teamId, list_id: listId , task_borders_color: taskBordersColor});

        // Clear form
        target.parentNode[0].value = '';
    }
});