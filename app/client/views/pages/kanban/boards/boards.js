Template.boards.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Teams');
        self.subscribe('Users');
    });
});

Template.boards.helpers({
    boardsTitle: function() {
        return TAPi18n.__('boards_page.title');
    },
    boards: function() {
        const userId = Meteor.user()._id;
        const user = Users.findOne(userId);
        const boards = Teams.find({ $or: [{ members: userId }, { leader: userId }]});
        
        return boards;
    }
});