    Template.teamKanban.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var teamId = FlowRouter.getParam('teamId');
        self.subscribe('Teams');
        self.subscribe('Tasks');
        self.subscribe('Users');
        self.subscribe('Lists');
        self.subscribe('Contacts');
        self.subscribe('Comments');
    });
});
Template.teamKanban.helpers({
    kanbanCategory: function () {
        return {
            title: TAPi18n.__('team_kanban_page.title'),
            path: "/kanban"
        }
    },
    team: function () {
        var team_id = FlowRouter.getParam('teamId');
        var currentTeam = Teams.findOne(team_id);
        return currentTeam;
    },
    kanbanBoardLists: function (team) {
        return Lists.find({ team_id: team._id });
    }
});
