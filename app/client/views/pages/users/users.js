Template.users.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Users');
    });
});

Template.users.helpers({
    usersTitle: function () {
        return TAPi18n.__('users_page.title');
    },
    users: function () {
        return Users.find({});
    },
    userEmail: function(){
        return this.emails[0].address;
    }
});