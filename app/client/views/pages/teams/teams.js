Template.teams.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Teams');
        self.subscribe('Contacts');
        self.subscribe('Lists');
        self.subscribe('Users');
    });
});

Template.teams.helpers({

    teamsTitle: function () {
        return TAPi18n.__('teams_page.title');
    },
    teams: function () {
        return Teams.find({});
    },
    createTeam: function() {
        return {
            buttonId: 'addTeam',
            buttonText: TAPi18n.__('teams_page.add_team'),
            collection: 'Teams',
            saveBtn: TAPi18n.__('common.add_save_btn'),
            title: TAPi18n.__('teams_page.add_team')
        };
    }

});