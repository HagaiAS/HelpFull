Template.teamCard.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Users');
        self.subscribe('Tasks');
    });
});

Template.teamCard.helpers({
    withoutLeader: function (members, leader) {
         return (_.filter(members, function(member){ return member !== leader; }));
    },
    userPicture: function (userId) {
        var users = Users.findOne(userId);
        return users.profile.picture;
    },
    teamTask: function (team) {
        var teamId = team._id;
        var notDoneTasks = Tasks.find({team_id: teamId, done: false}).count();
        var doneTasks = Tasks.find({team_id: teamId, done: true}).count();
        var allTasks = notDoneTasks + doneTasks;
        Session.set('allTasks#'+teamId, allTasks);
        Session.set('doneTasks#'+teamId, doneTasks);
        return doneTasks+'/'+allTasks;
    },
    teamTaskPercent: function (team) {
        var allTasksCount = Session.get('allTasks#'+team._id);
        var doneTasksCount = Session.get('doneTasks#'+team._id);
        var result = (doneTasksCount / allTasksCount) * 100;
        return Math.floor(result);
    }
});