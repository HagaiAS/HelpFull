Template.team.onCreated(function() {
    var self = this;
    self.autorun(function() {
        var teamId = FlowRouter.getParam('teamId');
        self.subscribe('Teams');
        self.subscribe('Users');
    });
});

Template.team.helpers({
    teamsCategory: function () {
        return {
            title: TAPi18n.__('teams_page.title'),
            path: "/teams"
        }
    },
    leaderTitle: function () {
        return '<span class="label label-success">ראש צוות</span>';
    },
    memberTitle: function () {
        return '<span class="label label-info">חבר צוות</span>';
    },
    leaderBorder: function () {
        return  'border-bottom-success';
    },
    memberBorder: function () {
        return  'border-bottom-info';
    },
    currentTeam: function () {
        var team_id = FlowRouter.getParam('teamId');
        var currentTeam = Teams.findOne(team_id);
        return currentTeam;
    },
    withoutLeader: function (members, leader) {
        return (_.filter(members, function(member){ return member !== leader; }));
    }
    // editTitle: function () {
    //     return TAPi18n.__('team_page.edit_title');
    // },
    // editBtn: function () {
    //     return TAPi18n.__('team_page.edit_save_btn');
    // },
    // docs: function () {
    //     return Collections.Contacts.find();
    // },
    // onError: function () {
    //     return function (error) {
    //         alert("BOO!"); console.log(error);
    //     };
    // },
    // onSuccess: function () {
    //     return function (result) {
    //         alert("YAY!"); console.log(result);
    //     };
    // },
    // beforeRemove: function () {
    //     return function (collection, id) {
    //         var doc = collection.findOne(id);
    //         var deleteContact = TAPi18n.__('team_page.delete_contact');
    //         if (confirmStep(deleteContact + doc.name + '?')) {
    //             this.remove();
    //         }
    //     };
    // }
});