Template.contacts.onCreated(function() {
    var self = this;
    self.autorun(function() {
        self.subscribe('Contacts');
    });
});

Template.contacts.helpers({
    
    title: function () {
        return TAPi18n.__('contacts_page.title');
    },
    createContact: function() {
        return {
            buttonId: 'addContact',
            buttonText: TAPi18n.__('contacts_page.add_contact'),
            collection: 'Contacts',
            saveBtn: TAPi18n.__('common.add_save_btn'),
            title: TAPi18n.__('contacts_page.add_contact')
        };
    }
});
