Template.tabular.onCreated(function() {
    $.extend(true, $.fn.dataTable.defaults, {
        language: {
            "emptyTable": TAPi18n.__('data_table.emptyTable'),
            "processing": TAPi18n.__('data_table.processing'),
            "lengthMenu": TAPi18n.__('data_table.lengthMenu'),
            "zeroRecords": TAPi18n.__('data_table.zeroRecords'),
            "info": TAPi18n.__('data_table.info'),
            "infoEmpty": TAPi18n.__('data_table.infoEmpty'),
            "infoFiltered": TAPi18n.__('data_table.infoFiltered'),
            "infoPostFix": TAPi18n.__('data_table.infoPostFix'),
            "loadingRecords": TAPi18n.__('data_table.loadingRecords'),
            "paginate": {
                "first": TAPi18n.__('data_table.paginate.first'),
                "previous": TAPi18n.__('data_table.paginate.previous'),
                "next": TAPi18n.__('data_table.paginate.next'),
                "last": TAPi18n.__('data_table.paginate.last')
            }
        },
        "fnDrawCallback": function (oSettings) {
            $('.dataTables_scrollBody').slimScroll({
                height: 'auto'
            });
        }
    });
});
