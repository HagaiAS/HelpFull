// Run this when the meteor app is started
Meteor.startup(function () {
    TAPi18n.setLanguage("he")
        .fail(function (error_message) {
            // Handle the situation
            console.log(error_message);
        });

    TimeSync.loggingEnabled = false;
    Template.myAtInput.replaces("atInput");
});