Schemas = this.Schemas || {};

Schemas.Address = new SimpleSchema({
    fullAddress: {
        label: "כתובת",
        type: String
    },
    lat: {
        type: Number,
        decimal: true
    },
    lng: {
        type: Number,
        decimal: true
    },
    geometry: {
        type: Object,
        blackbox: true
    },
    placeId: {
        type: String,
        optional: true
    },
    street: {
        type: String,
        label: "רחוב",
        optional: true,
        max: 100
    },
    city: {
        type: String,
        label: "עיר",
        optional: true,
        max: 50
    },
    state: {
        type: String,
        optional: true
    },
    zip: {
        type: String,
        label: "מיקוד",
        optional: true
    },
    country: {
        type: String
    }
});