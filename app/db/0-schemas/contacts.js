// for debugging errors from the schema.
this.SimpleSchema.debug = true;

Schemas = this.Schemas || {};

Schemas.Contacts = new SimpleSchema({
    name: {
        type: String,
        autoform: {
            label: "שם מלא",
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-user',
            left: true
        }
    },
    mobile: {
        type: String,
        optional: true,
        autoform: {
            label: "טלפון נייד",
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-mobile',
            left: true
        }
    },
    phone: {
        type: String,
        optional: true,
        autoform: {
            label: "טלפון נייח",
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-phone',
            left: true
        }
    },
    id_number: {
        type: String,
        index: true, // This creates ascending index for this field.
        unique: true, //This makes sure that this field has a unique index
        autoform: {
            label: "* תעודת זהות",
            // placeholder: "הכנס את תעודת זהות",
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-barcode',
            left: true
        }

        // max: 20 //Maximum allowed length of this field.
    },
    birth_date: {
        type: Date,
        optional: true,
        autoform: {
            label: "תאריך לידה",
            placeholder: "  /  /    ",
            // template : 'smalt-addon-icon',
            // icon: 'fa fa-fw fa-birthday-cake',
            // left: true
        }
    },
    gender: {
        type: String,
        optional: true,
        autoform: {
            label: "מין",
            // type: "smalt-addon-icon",
            // icon: 'fa fa-fw fa-venus-mars',
            // left: true,
            type: "select-radio-inline",
            options: function () {
                return [
                    {label: "♂ זכר ", value: "Male"},
                    {label: "♀ נקבה ", value: "Female"},
                    {label: " אחר ", value: "Other"}
                ];
            }
        }
    },
    address: {
        type: Schemas.Address,
        label: 'כתובת מגורים',
        optional: true,
        autoform: {
            type: 'googleplace',
            afFieldInput: {
                autocomplete: "off"
            }
        }
        // geopointName: "myOwnGeopointName" //optional, you can use a custom geopoint name
    }
});