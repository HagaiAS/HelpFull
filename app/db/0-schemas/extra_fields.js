Schemas = this.Schemas || {};

Schemas.ExtraFields = new SimpleSchema({
    floor_number: {
        label: "קומת מגורים",
        type: Number,
        optional: true
    },
    have_elevator: {
        label: "יש מעלית ?",
        type: Boolean,
        defaultValue: false,
        optional: true,
        autoform: {
            type: "boolean-checkbox-custom"
        }
    },
    num_of_living: {
        label: "מס' הנפשות בבית",
        type: Number,
        optional: true
    },
    another_disabled: {
        label: "נכה נוסף בבית?",
        type: Boolean,
        defaultValue: false,
        optional: true,
        autoform: {
            type: "boolean-checkbox-custom",
        }
    },
    disability_type: {
        label: "סוג מגבלה",
        type: String,
        optional: true
    },
    utilities: {
        label: "עזרים רפואיים",
        type: String,
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea"
            }
        }
    },
    safe_space: {
        label: "מרחב ביטחון",
        type: String,
        optional: true
    },
    has_public_shelter: {
        type: Boolean,
        defaultValue: false,
        optional: true,
        autoform: {
            label: "מקלט ציבורי ?",
            type: "boolean-checkbox-custom"
        }
    },
    distance_to_ps: {
        type: Number,
        decimal: true,
        optional: true,
        label: "מרחק למקלט ציבורי",
    },
    key_holder_ps: {
        type: String,
        label: "רכז מקלט ציבורי",
        optional: true
    },
    emergency_needs: {
        type: String,
        label: "צרכים בחירום",
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea"
            }
        }
    },
    extra_needs: {
        type: String,
        label: "צרכים נוספים",
        optional: true,
        autoform: {
            afFieldInput: {
                type: "textarea"
            }
        }
    }
});