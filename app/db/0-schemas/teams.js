Schemas = this.Schemas || {};

Schemas.Teams = new SimpleSchema({
    name: {
        type: String,
        label: 'שם הצוות',
    },
    description: {
        type: String,
        label: 'תיאור',
        optional: true
    },
    members: {
        type: [String],
        label: 'אנשי צוות',
        autoform: {
            type: "universe-select",
            afFieldInput: {
                multiple: true,
                optionsPlaceholder: "בחר אנשי צוות...",
            },
            options: function () {
                return Users.find({}).map(function (user) {
                    return {label: user.profile.name, value: user._id};
                });
            }
        }
    },
    active: {
        type: Boolean,
        label: 'צוות פעיל?',
        autoform: {
            omit: true
        }
    },
    leader: {
        type: String,
        optional: true,
        label: 'ראש צוות',
        autoform: {
            type: "universe-select",
            afFieldInput: {
                multiple: false,
                optionsPlaceholder: "בחר ראש צוות...",
            },
            options: function () {
                return Users.find({}).map(function (user) {
                    return {label: user.profile.name, value: user._id};
                });
            }
        }
    },
    location: {
        type: Schemas.Address,
        label: 'אזור פעילות',
        optional: true,
        autoform: {
            type: 'googleplace',
            afFieldInput: {
                autocomplete: "off"
            }
        }
        // geopointName: "myOwnGeopointName" //optional, you can use a custom geopoint name
    }

});