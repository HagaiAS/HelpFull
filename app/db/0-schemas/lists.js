Schemas = this.Schemas || {};

Schemas.Lists = new SimpleSchema({
    title: {
        type: String,
    },
    sort: {
        type: Number,
        decimal: true,
        // XXX We should probably provide a default
        optional: true,
        autoform: {
            omit: true
        }
    },
    team_id: {
        type: String,
        autoform: {
            omit: true
        }
    },
    badge_color: {
        type: String,
        autoform: {
            omit: true
        }
    },
    bordor_top: {
        type: String,
        autoform: {
            omit: true
        }
    },
    icon: {
        type: String,
        autoform: {
            omit: true
        }
    },
    is_done_list: {
        type: Boolean,
        optional: true,
        autoform: {
            omit: true
        }
    }
});