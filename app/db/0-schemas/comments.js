
Schemas = this.Schemas || {};

Schemas.Comments = new SimpleSchema({
	text: {
		type: String,
        autoform: {
            label: false,
            afFieldInput: {
                type: "textarea"
            }
        }
	},
    sort: {
        type: Number,
        decimal: true,
        // XXX We should probably provide a default
        optional: true,
        autoform: {
            omit: true
        }
    },
    task_id: {
        type: String,
        autoform: {
            label: false,
            afFieldInput: {
                type: "hidden"
            }
        }
    }    
});