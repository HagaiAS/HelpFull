Schemas = this.Schemas || {};

Schemas.UserProfile = new SimpleSchema({
    picture: {
        type: String,
        optional: true,
        label: 'תמונת פרופיל',
        autoform: {
            afFieldInput: {
                type: 'cloudinary'
            }
        }        
    },
    name: {
        type: String,
        label: 'שם מלא',
        autoform: {
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-user',
            left: true
        }
    },
    mobile: {
        type: String,
        label: 'נייד',
        optional: true,
        autoform: {
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-mobile',
            left: true
        }
    },
    phone: {
        type: String,
        label: 'טלפון',
        optional: true,
        autoform: {
            type: 'smalt-addon-icon',
            icon: 'fa fa-fw fa-phone',
            left: true
        }
    }
});

Schemas.User = new SimpleSchema({
    emails: {
        type: [Object],
        optional: true,
        autoform: {
            omit: true
        }
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean,
        autoform: {
            omit: true
        }
    },
    createdAt: {
        type: Date,
        autoform: {
            omit: true
        }
    },
    profile: {
        type: Schemas.UserProfile,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true,
        autoform: {
            omit: true
        }
    },
    roles: {
        type: [String],
        blackbox: true,
        optional: true,
        autoform: {
            omit: true
        }
    },
    status:{
        type: Object,
        blackbox: true,
        optional: true,
        autoform: {
            omit: true
        }
    }
});

