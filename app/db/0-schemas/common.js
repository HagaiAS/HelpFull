Schemas = this.Schemas || {};

Schemas.Common = new SimpleSchema({
    created_at: {
        type: Date,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert) {
                return new Date();
            } else if (this.isUpsert) {
                return {$setOnInsert: new Date()};
            } else {
                this.unset();
            }
        }
    },
    created_by: {
        type: String,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isInsert) {
                return this.userId;
            }
            else if (this.isUpsert) {
                return {$setOnInsert: this.userId};
            } else {
                this.unset();
            }
        }
    },
    updated_at: {
        type: Date,
        denyInsert: true,
        optional: true,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isUpdate) {
                return new Date();
            }
        }
    },
    updated_by: {
        type: String,
        denyInsert: true,
        optional: true,
        autoform: {
            omit: true
        },
        autoValue: function () {
            if (this.isUpdate || this.isUpsert) {
                return this.userId;
            }
        }
    }
});