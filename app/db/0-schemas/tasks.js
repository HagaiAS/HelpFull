Schemas = this.Schemas || {};

Schemas.Tasks = new SimpleSchema({
	title: {
		type: String,
        label: "הפנייה"
	},
    description: {
        type: String,
        optional: true,
        label: "תיאור",
        autoform: {
            afFieldInput: {
                type: "textarea"
            }
        }
    },
    due_date: {
        type: Date,
        optional: true,
        label: "תאריך יעד",
        autoform: {
            afFieldInput: {
                type: "bootstrap-datetimepicker"
            }
        }
    },
    border_label: {
        type: String,
        optional: true,
        autoform: {
            omit: true
        }
    },
    need_assistance_id: {
        type: String,
        optional: true,
        label: 'בחר עמית',
        autoform: {
            type: "universe-select",
            afFieldInput: {
                multiple: false,
                optionsPlaceholder: "בחר עמית מהרשימה...",
            },
            options: function () {
                return Contacts.find({}).map(function (contact) {
                    return {label: contact.name, value: contact._id};
                });
            }
        }
    },
    sort: {
        type: Number,
        decimal: true,
        // XXX We should probably provide a default
        optional: true,
        autoform: {
            omit: true
        }
    },
    is_urgent: {
		type: Boolean,
        defaultValue: false,
		optional: true,
        label: "האם דחוף?",
        autoform: {
            type: "boolean-checkbox-custom"
        }
	},
    task_borders_color: {
        type: String,
        optional: true,
        defaultValue: "warning-element",
        autoform: {
            omit: true
        }
    },
    team_id: {
        type: String,
        label: 'בחר צוות',
        autoform: {
            type: "universe-select",
            afFieldInput: {
                multiple: false,
                optionsPlaceholder: "בחר צוות...",
            },
            options: function () {
                return Teams.find({}).map(function (team) {
                    return {label: team.name, value: team._id};
                });
            }
        }
    },
    list_id: {
        type: String,
        optional: true,
        autoform: {
            omit: true
        },
        autoValue: function() {
            var team_id = this.field("team_id");
            if (this.isUpdate && team_id && team_id.isSet) {
                var current_list_id = Lists.findOne(
                    {
                        team_id: team_id.value,
                        sort: 1
                    })._id;
                return current_list_id;
            }
        }
    },
    done: {
        type: Boolean,
        optional: true,
        autoform: {
            omit: true
        },
        autoValue: function() {
            var listIdField = this.field("list_id");
            if (listIdField && listIdField.isSet) {
                var is_in_done_list = Lists.findOne({ _id: listIdField.value }).is_done_list;
                if (is_in_done_list)
                {
                    return true;
                }
                return false;
            }
        }
    }
});