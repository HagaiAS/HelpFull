TabularTables = this.TabularTables || {};
Meteor.isClient && Template.registerHelper('TabularTables', TabularTables);

var translatedColumns =
    [
        {data: "name", title: "שם מלא", class: "col-md-2", className: "all"},
        {data: "id_number", title: "תעודת זהות", class: "col-md-1 text-center", className: "all"},
        {data: "address.fullAddress", title: "כתובת", class: "col-md-2", className: "tablet-l"},
        {data: "mobile", title: "נייד", class: "col-md-1 text-center", className: "tablet-l"},
        {data: "phone", title: "טלפון", class: "col-md-1 text-center", className: "tablet-l"},
        {
            data: "birth_date",
            title: "תאריך לידה",
            class: "col-md-1 text-center",
            className: "tablet-l",
            render: function (val, type, doc) {
                if (val instanceof Date) {
                    return moment(val).calendar();
                }
            }
        },
        {
            data: "gender",
            title: "מין",
            class: "col-md-1 text-center",
            className: "desktop",
            render: function (val, type, doc) {
                if (val === 'Male') {
                    return "♂ זכר";
                }
                else if (val === 'Female') {
                    return "♀ נקבה";
                }
                else {
                    return "אחר";
                }
            }
        },
        {
            title: "אפשרויות",
            tmpl: Meteor.isClient && Template.contactsActionBtns,
            class: "col-md-1 text-center",
            className: "all"
        }
    ];

TabularTables.contactsTable = new Tabular.Table({
    name: "contactsTable",
    collection: Contacts,
    responsive: true,
    autoWidth: false,
    scrollY: '58vh',
    scrollCollapse: true,
    limit: 100,
    paging: false,
    select: {
        style: 'single'
    },
    dom: '<"pull-right"f><"pull-left"B>rt',
    buttons: [
        {
            extend:    'excelHtml5',
            text:      '<img src="/icons/ms_excel-24.png" width="20">',
            titleAttr: 'ייצא לאקסל'
        },
        {
            extend: 'print',
            text: '<img src="/icons/print-24.png" width="20">',
            titleAttr: 'הדפס טבלה'
        },
        {
            extend: 'colvis',
            text: '<img src="/icons/show_property-32.png" width="20">',
            titleAttr: 'עמודות להצגה',
            columns: ':gt(0)'
        }
    ],
    'language': {
        search: '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>',
    },
    columns: translatedColumns
});

