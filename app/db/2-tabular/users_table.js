TabularTables = this.TabularTables || {};
Meteor.isClient && Template.registerHelper('TabularTables', TabularTables);

var translatedColumns =
    [
        {
            data: "status.online",
            title: "מחובר",
            class: "text-center sorting",
            //orderable: false,
            width: "18px",
            render: function (val, type, doc) {
                if (val) {
                    return '<img src="/icons/ok-24.png" width="16">';
                }
            }
        },
        {
            data: "profile.name",
            title: "שם מלא",
            // class: "col-md-2",
            render: function (val, type, doc) {
                const profileImg = '<img class="user-avatar img-circle" src="'+  doc.profile.picture +'" alt="user profile">';
                const result = '<a href="/profile/'+ doc._id +'">' + profileImg + '   ' + val + '</a>';
                return result;
            }
        },
        {data: "profile.mobile", title: "נייד", class: "text-center"},
        {data: "profile.phone", title: "טלפון", class: "text-center"},
        // {data: "emails[0].address", title: 'דוא"ל', class: "text-center"},
        {
            data: "createdAt",
            title: "הצטרף בתאריך",
            class: "text-center",
            render: function (val, type, doc) {
                if (val instanceof Date) {
                    return moment(val).calendar();
                }
            }
        }
        // {
        //     title: "אפשרויות",
        //     tmpl: Meteor.isClient && Template.requestsActionBtns,
        //     className: "all",
        //     class: "col-md-1 text-center"
        // }
    ];

TabularTables.usersTable = new Tabular.Table({
    name: "usersTable",
    collection: Users,
    responsive: true,
    autoWidth: false,
    // This is the important part, read the documentation on the official site for more info
    dom: '<"pull-right"f><"pull-left"B>rt',
    scrollY: '58vh',
    scrollCollapse: true,
    limit: 100,
    paging: false,
    buttons: [
        {
            extend: 'excelHtml5',
            text: '<img src="/icons/ms_excel-24.png" width="20">',
            titleAttr: 'ייצא לאקסל'
        },        
        {
            extend: 'print',
            text: '<img src="/icons/print-24.png" width="20">',
            titleAttr: 'הדפס טבלה'
        },
        {
            extend: 'colvis',
            text: '<img src="/icons/show_property-32.png" width="20">',
            titleAttr: 'עמודות להצגה',
            //columns: ':gt(0)'
        }        
    ],
    'language': {
        search: '<div class="input-group"><span class="input-group-addon">' +
        '<span class="glyphicon glyphicon-search"></span></span>',
    },
    columns: translatedColumns
});

