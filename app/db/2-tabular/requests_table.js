TabularTables = this.TabularTables || {};
Meteor.isClient && Template.registerHelper('TabularTables', TabularTables);

var translatedColumns =
    [
        {
            data: "title",
            title: "כותרת",
            class: "col-md-2",
            className: "all",
            render: function (val, type, doc) {
                return '<a href="/kanban/'+ doc.team_id +'">'+ val +'</a>';
            }
        },
        {data: "description", title: "תיאור", class: "col-md-3", className: "tablet-l"},
        {
            data: "due_date",
            title: "תאריך יעד",
            class: "col-md-1 text-center",
            render: function (val, type, doc) {
                if (val instanceof Date) {
                    return moment(val).calendar();
                }
            }
        },
        {
            data: "team()",
            title: "צוות מטפל",
            class: "col-md-1 sorting text-center",
            className: "all",
            render: function (val, type, doc) {
                return '<a href="/team/'+ doc.team_id +'">'+ val +'</a>';
            }
        },
        {
            data: "need()",
            title: "פנייה מעמית",
            class: "col-md-1 sorting text-center",
            className: "tablet-l"
        },
        {
            data: "is_urgent",
            title: "האם דחוף?",
            class: "col-md-1 text-center",
            className: "tablet-l",
            render: function (val, type, doc) {
                if (val) {
                    return 'כן';
                }
                else {
                    return 'לא';
                }
            }
        },
        {
            title: "אפשרויות",
            tmpl: Meteor.isClient && Template.requestsActionBtns,
            className: "all",
            class: "col-md-1 text-center"
        }
    ];

TabularTables.requestsTable = new Tabular.Table({
    name: "requestsTable",
    collection: Tasks,
    responsive: true,
    autoWidth: false,
    scrollY: '58vh',
    scrollCollapse: true,
    limit: 100,
    paging: false,
    // This is the important part, read the documentation on the official site for more info
    dom: '<"pull-right"f><"pull-left"B>rt',
    buttons: [
        {
            extend:    'excelHtml5',
            text:      '<img src="/icons/ms_excel-24.png" width="20">',
            titleAttr: 'ייצא לאקסל'
        },        
        {
            extend: 'print',
            text: '<img src="/icons/print-24.png" width="20">',
            titleAttr: 'הדפס טבלה'
        },
        {
            extend: 'colvis',
            text: '<img src="/icons/show_property-32.png" width="20">',
            titleAttr: 'עמודות להצגה',
            columns: ':gt(0)'
        }        
    ],
    'language': {
        search: '<div class="input-group"><span class="input-group-addon"><span class="glyphicon glyphicon-search"></span></span>',
    },
    extraFields: ['need_assistance_id', 'team_id'],
    columns: translatedColumns
});

