Contacts = new Mongo.Collection('contacts');

Contacts.allow({
    insert: function (userId, doc) {
        // add custom authentication code here
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        // add custom authentication code here
        return true;
    },

    remove: function (userId, doc) {
        // add custom authentication code here
        return true;
    }
});

Schemas.Contacts.i18n("contacts_schema");

Contacts.attachSchema([
    Schemas.Contacts,
    Schemas.ExtraFields,
    Schemas.Common
]);
