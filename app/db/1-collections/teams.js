Teams = new Mongo.Collection('teams');

Teams.allow({
    insert: function (userId, doc) {
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        return true;
    },

    remove: function (userId, doc) {
        return true;
    }
});

Teams.attachSchema([
    Schemas.Teams,
    Schemas.Common
]);

Teams.before.insert(function (userId, doc) {
    doc.active = true;
});

if (Meteor.isServer) {
    Teams.after.insert(function (userId, doc, fieldNames, modifier, options) {
        Lists.insert({
            title: 'התקבלו',
            sort: 1,
            team_id: doc._id,
            badge_color: 'badge-danger',
            bordor_top: 'border-top-danger',
            icon: 'fa-pencil-square-o',
            is_done_list: false
        });

        Lists.insert({
            title: 'בביצוע',
            sort: 2,
            team_id: doc._id,
            badge_color: 'badge-warning',
            bordor_top: 'border-top-warning',
            icon: 'fa-hand-o-down',
            is_done_list: false
        });

        Lists.insert({
            title: 'בוצעו',
            sort: 3,
            team_id: doc._id,
            badge_color: 'badge-primary',
            bordor_top: 'border-top-primary',
            icon: 'fa-check-circle',
            is_done_list: true
        });
    }, {fetchPrevious: false});
}