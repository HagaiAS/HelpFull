Tasks = new Mongo.Collection('tasks');

Tasks.allow({
    insert: function (userId, doc) {
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        return true;
    },

    remove: function (userId, doc) {
        return true;
    }
});

Tasks.attachSchema([
    Schemas.Tasks,
    Schemas.Common
]);

Tasks.helpers({
    team: function() {
        var team = Teams.findOne(this.team_id);
        if (team) {
            return team.name;
        }
        else {
            return this.team_id;
        }
    },
    need: function() {
        var contact = Contacts.findOne(this.need_assistance_id);
        if (contact) {
            return contact.name;
        }
        else {
            return contact;
        }
    }
});
