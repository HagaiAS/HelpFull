Users = Meteor.users;

Users.allow({
    insert: function (userId, doc) {
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        return true;
    },

    remove: function (userId, doc) {
        return true;
    }
});

Users.attachSchema(Schemas.User);