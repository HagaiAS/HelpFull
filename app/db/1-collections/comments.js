Comments = new Mongo.Collection('comments');

Comments.allow({
    insert: function (userId, doc) {
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        return true;
    },

    remove: function (userId, doc) {
        return true;
    }
});

Comments.attachSchema([
    Schemas.Comments,
    Schemas.Common
]);
