Lists = new Mongo.Collection('lists');

Lists.allow({
    insert: function (userId, doc) {
        // add custom authentication code here
        return true;
    },

    update: function (userId, doc, fields, modifier) {
        // add custom authentication code here
        return true;
    },

    remove: function (userId, doc) {
        // add custom authentication code here
        return true;
    }
});

Lists.attachSchema([
    Schemas.Lists,
    Schemas.Common
]);