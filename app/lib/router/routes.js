FlowRouter.route('/intro', {
    name: "intro",
    action: function () {
        BlazeLayout.render('introLayout', {
            main: "intro"
        });
    }
});

FlowRouter.route('/', {
    name: "home",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "home",
            nav: "nav"
        });
    }
});

FlowRouter.route('/about', {
    name: "about",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "about",
            nav: "nav"
        });
    }
});

FlowRouter.notFound = {
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "pageNotFound",
            nav: "nav"
        });
    }
};

var privateRoutes = FlowRouter.group({
    name: 'private',
    triggersEnter: [AccountsTemplates.ensureSignedIn]
});


privateRoutes.route('/profile', {
    name: 'currentUserProfile',
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "profile",
            nav: "nav"
        });
    }
});

privateRoutes.route('/profile/:userId', {
    name: 'profile',
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "profile",
            nav: "nav"
        });
    }
});

privateRoutes.route('/contacts', {
    name: "contacts",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "contacts",
            nav: "nav"
        });
    }
});

privateRoutes.route('/kanban', {
    name: "boards",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "boards",
            nav: "nav"
        });
    }
});

privateRoutes.route('/kanban/:teamId', {
    name: "teamKanban",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "teamKanban",
            nav: "nav"
        });
    }
});

privateRoutes.route('/tasks/:taskId', {
    name: "teamKanban",
    action: function () {
        BlazeLayout.render('modalLayout', {
            footer: "taskModalFooter",
            main: "taskDetailsModal",
            header: "taskModalHeader"
        });
    }
});

privateRoutes.route('/teams', {
    name: "teams",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "teams",
            nav: "nav"
        });
    }
});

privateRoutes.route('/team/:teamId', {
    name: "team",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "team",
            nav: "nav"
        });
    }
});

privateRoutes.route('/requests', {
    name: "requests",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "requests",
            nav: "nav"
        });
    }
});

privateRoutes.route('/users', {
    name: "users",
    action: function () {
        BlazeLayout.render('masterLayout', {
            footer: "footer",
            main: "users",
            nav: "nav"
        });
    }
});

//Routes
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('enrollAccount');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('verifyEmail');
