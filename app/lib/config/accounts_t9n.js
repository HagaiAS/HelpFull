T9n.setLanguage('he');

T9n.map('he', {
    email: 'דואר אלקטרוני',
    name: 'שם מלא',
    mobile: 'נייד',
    phone: 'טלפון'   
});