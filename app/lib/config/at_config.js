// Options
AccountsTemplates.configure({
    defaultLayout: 'modalLayout',
    defaultLayoutRegions: {
        header: 'modalHeader',
        footer: 'footer'
    },
    defaultContentRegion: 'main',
    showForgotPasswordLink: true,
    overrideLoginErrors: true,
    enablePasswordChange: true,
    showLabels: false,
    sendVerificationEmail: false,
    // enforceEmailVerification: true,
    confirmPassword: true,
    focusFirstInput: false,
    //continuousValidation: false,
    //displayFormLabels: true,
    //forbidClientAccountCreation: true,
    //formValidationFeedback: true,
    homeRoutePath: '/',
    //showAddRemoveServices: false,
    showPlaceholders: true,
    negativeValidation: true,
    positiveValidation: true,
    negativeFeedback: true,
    positiveFeedback: true,

    // Privacy Policy and Terms of Use
    privacyUrl: 'privacy',
    termsUrl: 'terms-of-use'
});

AccountsTemplates.removeField('email');
AccountsTemplates.removeField('password');

AccountsTemplates.addFields([
    {
        _id: 'email',
        type: 'email',
        required: true,
        placeholder: "email",
        re: /.+@(.+){2,}\.(.+){2,}/,
        errStr: 'error.emailRequired',
        options: {
            icon: "📧"
        }
    },
    {
        _id: "name",
        type: "text",
        minLength: 3,
        placeholder: "name",
        required: true,
        trim: true,
        options: {
            faIcon: "fa-user"
        },
        errStr: 'error.minChar'
    },
    {
        _id: "mobile",
        type: "tel",
        placeholder: "mobile",
        options: {
            icon: "📱"
        },
        required: true
    },
    {
        _id: "phone",
        type: "tel",
        placeholder: "phone",
        options: {
            faIcon: "fa-phone"
        }
    },
    {
        _id: 'password',
        type: 'password',
        placeholder: 'password',
        required: true,
        minLength: 6,
        options: {
            icon: "🔐"
        }
    },
    {
        _id: 'picture',
        type: 'hidden'
    }
]);